using UnityEngine;
using System.Collections;

public struct QuaternionEx {
	public	float	x, y, z, w;		// w corresponds to the real part.
	
	// for compatibility
	public Quaternion convert {
		get {
			return new Quaternion(x, y, z, w);
		}
	}
	
	// for compatibility
	public static QuaternionEx Convert(Quaternion quaternion) {
		QuaternionEx q = new QuaternionEx();
		q.x = quaternion.x;
		q.y = quaternion.y;
		q.z = quaternion.z;
		q.w = quaternion.w;
		return q;
	}
	
	// radian [0 ... Pi]
	public static QuaternionEx QuaternionFromAxisAndRadian(Vector3 axis, float radian) {
		float halfAngle = radian * 0.5f;
		float scale		= Mathf.Sin(halfAngle);	
		
		QuaternionEx q;
		q.x = scale * axis.x;
		q.y = scale * axis.y;
		q.z = scale * axis.z;
		q.w = Mathf.Cos(halfAngle);
		
		return q;
 	}
	
	public static QuaternionEx QuaternionFromVectors(Vector3 fromVector, Vector3 toVector) {
		fromVector = fromVector.normalized;
		toVector   = toVector.normalized;
		
		Vector3 axis   = Vector3.Cross(fromVector, toVector);
		float   radian = Mathf.Acos(Vector3.Dot(fromVector, toVector));		// radian [0 ... Pi]
		
		float mag = axis.magnitude;
		if (mag <= Mathf.Epsilon || System.Single.IsNaN (radian)) {
			axis   = Vector3.zero;
			radian = 0.0f;
		} else {
			axis = axis / mag;
		}
		
		return QuaternionEx.QuaternionFromAxisAndRadian(axis, radian);		
	}
	
	public static QuaternionEx QuaternionFromMatrix(Matrix3x3 mat) {
		float[] elem = new float[4];
		elem[0] = + mat[0, 0] - mat[1, 1] - mat[2, 2] + 1.0f;
        elem[1] = - mat[0, 0] + mat[1, 1] - mat[2, 2] + 1.0f;
		elem[2] = - mat[0, 0] - mat[1, 1] + mat[2, 2] + 1.0f;
		elem[3] = + mat[0, 0] + mat[1, 1] + mat[2, 2] + 1.0f;
        
		uint biggestIndex = 0;		

		for (uint i = 1; i < 4; ++ i) {
			if (elem[i] > elem[biggestIndex]) {
				biggestIndex = i;
			}
		}
            
		if (elem[biggestIndex] <= 0.0f) {
			// invalid matrix
			return new QuaternionEx();
		}

		float[]	q		= new float[4];
		float	vtmp	= Mathf.Sqrt(elem[biggestIndex]) * 0.5f;
		
		q[biggestIndex] = vtmp;
		
		float mult = 0.25f / vtmp;

		switch (biggestIndex) {
		case 0: // x
			q[1] = (mat[0, 1] + mat[1, 0]) * mult;
			q[2] = (mat[2, 0] + mat[0, 2]) * mult;
			q[3] = (mat[2, 1] - mat[1, 2]) * mult;
			break;
		case 1: // y
			q[0] = (mat[0, 1] + mat[1, 0]) * mult;
			q[2] = (mat[1, 2] + mat[2, 1]) * mult;
			q[3] = (mat[0, 2] - mat[2, 0]) * mult;
			break;
		case 2: // z
			q[0] = (mat[2, 0] + mat[0, 2]) * mult;
			q[1] = (mat[1, 2] + mat[2, 1]) * mult;
			q[3] = (mat[1, 0] - mat[0, 1]) * mult;
			break;
		case 3: // w
			q[0] = (mat[2, 1] - mat[1, 2]) * mult;
			q[1] = (mat[0, 2] - mat[2, 0]) * mult;
			q[2] = (mat[1, 0] - mat[0, 1]) * mult;
			break;
		}
		
		QuaternionEx newQuaternion;
		newQuaternion.x = q[0];
		newQuaternion.y = q[1];
		newQuaternion.z = q[2];
		newQuaternion.w = q[3];
		return newQuaternion;
	}
	
	public Vector3 axis {
		get {
			Vector3	vec = new Vector3(x, y, z);
			float	len	= Mathf.Sqrt(x * x + y * y + z * z);
			
			if (len <= Mathf.Epsilon) {
				return new Vector3(1.0f, 0.0f, 0.0f);
			} else {
				return vec / len;
			}
		}
	}
	
	// the value returned is in [0 ... Pi], which is the shorter one.
	public float angle {
		get {
			QuaternionEx q = this.normalize;
			float        w = Mathf.Abs (q.w);
			if (w < 1.0f) {
				float angle = Mathf.Acos (w) * 2.0f;
				return Mathf.Abs (angle);
			} else {
				return 0.0f;
			}
		}
	}
	
	public QuaternionEx inverse {
		get {
			return Inverse(this);
		}
	}
	
	public QuaternionEx normalize {
		get {
			return Normalize(this);
		}
	}
	
	public QuaternionEx conjugate {
		get {
			return Conjugate(this);
		}
	}
	
	public Matrix3x3 rotationMatrix3x3 {
		get {
			return RotationMatrix3x3(this);
		}
	}
	
	public Vector3 RotateVector(Vector3 vec) {
		QuaternionEx q = QuaternionEx.identity;
		q.x = vec.x;
		q.y = vec.y;
		q.z = vec.z;
		q.w = 0.0f;
		QuaternionEx result = this * q * this.conjugate;
		return new Vector3(result.x, result.y, result.z);
	}
	
	public static QuaternionEx identity {
		get {
			QuaternionEx q;
			q.x = 0.0f;
			q.y = 0.0f;
			q.z = 0.0f;
			q.w = 1.0f;
			return q;
		}
	}
	
	public static QuaternionEx zero {
		get {
			QuaternionEx q;
			q.x = 0.0f;
			q.y = 0.0f;
			q.z = 0.0f;
			q.w = 0.0f;
			return q;
		}
	}
	
	public static QuaternionEx Lerp(QuaternionEx left, QuaternionEx right, float t) {
		return QuaternionEx.Convert(Quaternion.Lerp(left.convert, right.convert, t)).normalize;
	}
	
	public static QuaternionEx Slerp(QuaternionEx left, QuaternionEx right, float t) {
		return QuaternionEx.Convert(Quaternion.Slerp(left.convert, right.convert, t)).normalize;
	}
	
	public static float DotProduct(QuaternionEx left, QuaternionEx right) {
		float a0 = left.x * right.x;
		float a1 = left.y * right.y;
		float a2 = left.z * right.z;
		float a3 = left.w * right.w;
		return a0 + a1 + a2 + a3;
	}

	public static QuaternionEx operator+ (QuaternionEx left, QuaternionEx right) {
		QuaternionEx result;
		result.x = left.x + right.x;
		result.y = left.y + right.y;
		result.z = left.z + right.z;
		result.w = left.w + right.w;
		return result;
	}

	public static QuaternionEx operator* (float t, QuaternionEx q) {
		QuaternionEx result;
		result.x = q.x * t;
		result.y = q.y * t;
		result.z = q.z * t;
		result.w = q.w * t;
		return result;
	}
	
	public static QuaternionEx operator* (QuaternionEx left, QuaternionEx right) {
		QuaternionEx result;
		
		result.x =
			left.w * right.x +
			left.x * right.w +
			left.y * right.z -
			left.z * right.y;
		
		result.y =
			left.w * right.y +
			left.y * right.w +
			left.z * right.x -
			left.x * right.z;
		
		result.z =
			left.w * right.z +
			left.z * right.w +
			left.x * right.y -
			left.y * right.x;
		
		result.w = 
			left.w * right.w -
			left.x * right.x -
			left.y * right.y -
			left.z * right.z;

		return result;
	}
	
	private static QuaternionEx Normalize(QuaternionEx q) {
		QuaternionEx result;
		float scale = 1.0f / Mathf.Sqrt(Norm (q));
		result.x = scale * q.x;
		result.y = scale * q.y;
		result.z = scale * q.z;
		result.w = scale * q.w;
		return result;
	}
	
	private static QuaternionEx Conjugate(QuaternionEx q) {
		QuaternionEx result;
		result.x = - q.x;
		result.y = - q.y;
		result.z = - q.z;
		result.w = + q.w;
		return result;
	}
	
	private static QuaternionEx Inverse(QuaternionEx q) {
		QuaternionEx result;
		float	scale	= 1.0f / Norm(q);
		result.x = - scale * q.x;
		result.y = - scale * q.y;
		result.z = - scale * q.z;
		result.w = + scale * q.w;
		return result;
	}
	
	private static float Norm(QuaternionEx q) {
		return q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w;
	}
	
	private static Matrix3x3 RotationMatrix3x3(QuaternionEx q) {
		QuaternionEx quaternion = q.normalize;
            
        float x = quaternion.x;
        float y = quaternion.y;
        float z = quaternion.z;
        float w = quaternion.w;
            
        float _2x = 2.0f * x;
        float _2y = 2.0f * y;
        float _2z = 2.0f * z;
        float _2w = 2.0f * w;
            
        Matrix3x3 m;
		
		m.m00 = 1.0f - _2y * y - _2z * z;
		m.m01 = _2x * y - _2w * z;
		m.m02 = _2x * z + _2w * y;
		
		m.m10 = _2x * y + _2w * z;
		m.m11 = 1.0f - _2x * x - _2z * z;
		m.m12 = _2y * z - _2w * x;
		
		m.m20 = _2x * z - _2w * y;
		m.m21 = _2y * z + _2w * x;
		m.m22 = 1.0f - _2x * x - _2y * y;

		return m;
	}

}
