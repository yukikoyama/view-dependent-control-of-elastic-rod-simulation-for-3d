using UnityEngine;
using System.Collections.Generic;

public class MatrixVL {
	
	private float[][] values = null;
	
	public int rowSize {
		get {
			return values.Length;
		}
	}
	
	public int colSize {
		get {
			return values[0].Length;
		}
	}
	
	public MatrixVL (int rowSize, int colSize) {
		values = MatrixDecomposition.MatrixDecompositionProgram.MatrixCreate(rowSize, colSize);
	}
	
	public MatrixVL (float[][] values) {
		this.values = values;
	}
	
	public float this [int row, int col] {
		get {
			return values[row][col];
		}
		set {
			values[row][col] = value;
		}
	}
	
	public MatrixVL inverse {
		get {
			float [][] result = MatrixDecomposition.MatrixDecompositionProgram.MatrixInverse(values);
			MatrixVL resultMatrix = new MatrixVL(result);
			return resultMatrix;
		}
	}
	
	public MatrixVL transpose {
		get {
			MatrixVL result = new MatrixVL(colSize, rowSize);
			for (int row = 0; row < rowSize; ++ row) {
				for (int col = 0; col < colSize; ++ col) {
					result[col, row] = this[row, col];
				}
			}
			return result;
		}
	}
	
	public static VectorVL operator* (MatrixVL mat, VectorVL vec) {
		if (mat.colSize != vec.size) {
			Debug.Log ("Invalid vector size.");
			return vec;
		}
		
		return new VectorVL(MatrixDecomposition.MatrixDecompositionProgram.MatrixVectorProduct(mat.values, vec.values));
	}
	
	public static MatrixVL operator* (MatrixVL left, MatrixVL right) {
		return new MatrixVL(MatrixDecomposition.MatrixDecompositionProgram.MatrixProduct(left.values, right.values));
	}
}

