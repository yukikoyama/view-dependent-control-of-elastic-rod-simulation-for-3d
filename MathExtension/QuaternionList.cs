using System.Collections.Generic;
using UnityEngine;

namespace ViewDependentRodSimulation {
	[System.Serializable]
	public class QuaternionList {
		public List<Quaternion> qList;
		
		public Quaternion this [int i] {
			get {
				return qList[i];
			}
			set {
				qList[i] = value;
			}
		}
		
		public QuaternionList() {
			qList = new List<Quaternion>();
		}
		
		public void Add(Quaternion q) {
			qList.Add(q);
		}
	}
}