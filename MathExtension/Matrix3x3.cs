using UnityEngine;
using System;


public struct Matrix3x3 {

	public float m00;
	public float m10;
	public float m20;
	public float m01;
	public float m11;
	public float m21;
	public float m02;
	public float m12;
	public float m22;
	
	public Matrix3x3(
		float m00, float m10, float m20,
		float m01, float m11, float m21,
		float m02, float m12, float m22) {
		this.m00 = m00;
		this.m01 = m01;
		this.m02 = m02;
		this.m10 = m10;
		this.m11 = m11;
		this.m12 = m12;
		this.m20 = m20;
		this.m21 = m21;
		this.m22 = m22;
	}
	
	public float this [int row, int column] {
		get {
			return this [row + column * 3];
		}
		set {
			this [row + column * 3] = value;
		}
	}
	
	public float this [int index] {
			get	{
				switch (index) {
					case 0: 
						return this.m00;
					case 1: 
						return this.m10;
					case 2:	
						return this.m20;
					case 3:	
						return this.m01;
					case 4: 
						return this.m11;
					case 5:
						return this.m21;
					case 6:
						return this.m02;
					case 7:
						return this.m12;
					case 8:
						return this.m22;
					default:
						throw new Exception ("Invalid matrix index!");
				}
			}
			set {
				switch (index) {
					case 0: {
						this.m00 = value;
						break;
					}
					case 1: {
						this.m10 = value;
						break;
					}
					case 2: {
						this.m20 = value;
						break;
					}
					case 3: {
						this.m01 = value;
						break;
					}
					case 4: {
						this.m11 = value;
						break;
					}
					case 5: {
						this.m21 = value;
						break;
					}
					case 6: {
						this.m02 = value;
						break;
					}
					case 7: {
						this.m12 = value;
						break;
					}
					case 8: {
						this.m22 = value;
						break;
					}
					default: {
						throw new IndexOutOfRangeException ("Invalid matrix index!");
					}
				}
			}
		}
	
	public Matrix3x3 inverse {
		get	{
			return Matrix3x3.Inverse(this);
		}
	}
	
	public Matrix3x3 transpose {
		get	{
			return Matrix3x3.Transpose(this);
		}
	}
	
	public float determinant {
		get {
	        return m00 * (m11 *m22 - m21 * m12)
				- m01 * (m10 * m22 - m20 * m12)
				+ m02 * (m10 * m21 - m20 * m11);
		}
	}
	
	//////////////////////////////////////////////
	// static
	//////////////////////////////////////////////
	
	public static Matrix3x3 zero {
		get {
			Matrix3x3 m = new Matrix3x3();
			for (int i = 0; i < 9; ++ i) {
				m[i] = 0.0f;
			}
			return m;
		}
	}
		
		public static Matrix3x3 identity {
			get {
				Matrix3x3 m = new Matrix3x3();
				for (int i = 0; i < 9; ++ i) {
					m[i] = 0.0f;
				}
				m[0] = 1.0f;
				m[4] = 1.0f;
				m[8] = 1.0f;
				return m;
			}
		}
		
	public static Matrix3x3 Inverse(Matrix3x3 matrix) {
		float s = matrix.determinant;
		if (s <= 0.0f) {
			Debug.Log("Matrix.Inverse failed.");
			return identity;
		}
		
		s = 1.0f / s;
		
		Matrix3x3 tmp;
		
		tmp.m00 = matrix.m11 * matrix.m22 - matrix.m12 * matrix.m21;
		tmp.m01 = matrix.m02 * matrix.m21 - matrix.m01 * matrix.m22;
		tmp.m02 = matrix.m01 * matrix.m12 - matrix.m02 * matrix.m11;
		tmp.m10 = matrix.m12 * matrix.m20 - matrix.m10 * matrix.m22;
		tmp.m11 = matrix.m00 * matrix.m22 - matrix.m02 * matrix.m20;
		tmp.m12 = matrix.m02 * matrix.m10 - matrix.m00 * matrix.m12;
		tmp.m20 = matrix.m10 * matrix.m21 - matrix.m11 * matrix.m20;
		tmp.m21 = matrix.m01 * matrix.m20 - matrix.m00 * matrix.m21;
		tmp.m22 = matrix.m00 * matrix.m11 - matrix.m01 * matrix.m10;
		
		tmp = s * tmp;
		
		return tmp;
	}
		
	
	public static Matrix3x3 Transpose(Matrix3x3 matrix) {
		Matrix3x3 result = new Matrix3x3();
		for (int i = 0; i < 3; ++ i) {
			for (int j = 0; j < 3; ++ j) {
				result[i, j] = matrix[j, i];
			}
		}
		return result;
	}
				
		//////////////////////////////////////////////
		// overload
		//////////////////////////////////////////////

		public static Matrix3x3 operator+ (Matrix3x3 left, Matrix3x3 right) {
			Matrix3x3 result = new Matrix3x3();
			for (int i = 0; i < 9; ++ i) {
				result[i] = left[i] + right[i];
			}
			return result;
		}

		public static Matrix3x3 operator- (Matrix3x3 left, Matrix3x3 right) {
			Matrix3x3 result = new Matrix3x3();
			for (int i = 0; i < 9; ++ i) {
				result[i] = left[i] - right[i];
			}
			return result;
		}

		public static Matrix3x3 operator* (Matrix3x3 left, Matrix3x3 right) {
			Matrix3x3 result = new Matrix3x3();
			for (int i = 0; i < 3; ++ i) {
				for (int j = 0; j < 3; ++ j) {
					float sum = 0.0f;
					for (int k = 0; k < 3; ++ k) {
						sum += left[i, k] * right[k, j];
					}
					result[i, j] = sum;
				}
			}
			return result;
		}
		
	public static Matrix3x3 operator* (float scale, Matrix3x3 matrix) {
		Matrix3x3 result = new Matrix3x3();
		for (int i = 0; i < 9; ++ i) {
			result[i] = scale * matrix[i];
		}
		return result;
	}
	
	public static Vector3 operator* (Matrix3x3 mat, Vector3 vec) {
		Vector3 result = Vector3.zero;
		for (int i = 0; i < 3; ++ i) {
			for (int j = 0; j < 3; ++ j) {
				result[i] += mat[i, j] * vec[j];
			}
		}
		return result;
	}
}