using UnityEngine;
using System;

public class Jacobi {
	public	Matrix3x3	matrix;
	public	Vector3		eigenValues;
	public	Matrix3x3 	eigenVectors;
	
	public	int		maxSweeps	= 5;
	public	float	tol			= 1e-10f;
	
	private	Matrix3x3	tmp;
	 
	public void Do() {
		float onorm, dnorm;
        float b, dma, q, t, c, s;
        float atemp, vtemp, dtemp;
        int i, j, k, l;
		
		float[] a = new float[9];
		float[] v = new float[9];
		float[] d = new float[3];
		tmp = matrix.transpose;
		for (int m = 0; m < 9; ++ m) {
			a[m] = tmp[m];
		}
		tmp = eigenVectors.transpose;
		for (int m = 0; m < 9; ++ m) {
			v[m] = tmp[m];
		}
       	
        d[0] = a[0];
        d[1] = a[4];
        d[2] = a[8];
        
        for (l = 1; l <= maxSweeps; ++ l) {
			dnorm = Mathf.Abs(d[0]) + Mathf.Abs(d[1]) + Mathf.Abs(d[2]);
			onorm = Mathf.Abs(a[1]) + Mathf.Abs(a[2]) + Mathf.Abs(a[5]);
            // Normal end point of this algorithm.
            if((onorm / dnorm) <= tol) {
				Update(d, v);
                return;
            }
            
            for (j = 1; j < 3; j++) {
                for (i = 0; i <= j - 1; i++) {
                    b = a[3 * i+j];
                    if(Mathf.Abs(b) > 0.0f) {
                        dma = d[j] - d[i];
                        if((Mathf.Abs(dma) + Mathf.Abs(b)) <= Mathf.Abs(dma)) {
                            t = b / dma;
                        } else {
                            q = 0.5f * dma / b;
                            t = 1.0f/(Mathf.Abs(q) + Mathf.Sqrt(1.0f+q*q));
                            if (q < 0.0)
                                t = -t;
                        }
                        
                        c = 1.0f/Mathf.Sqrt(t*t + 1.0f);
                        s = t * c;
                        a[3*i+j] = 0.0f;
                        
                        for (k = 0; k <= i-1; k++) {
                            atemp = c * a[3*k+i] - s * a[3*k+j];
                            a[3*k+j] = s * a[3*k+i] + c * a[3*k+j];
                            a[3*k+i] = atemp;
                        }
                        
                        for (k = i+1; k <= j-1; k++) {
                            atemp = c * a[3*i+k] - s * a[3*k+j];
                            a[3*k+j] = s * a[3*i+k] + c * a[3*k+j];
                            a[3*i+k] = atemp;
                        }
                        
                        for (k = j+1; k < 3; k++) {
                            atemp = c * a[3*i+k] - s * a[3*j+k];
                            a[3*j+k] = s * a[3*i+k] + c * a[3*j+k];
                            a[3*i+k] = atemp;
                        }
                        
                        for (k = 0; k < 3; k++) {
                            vtemp = c * v[3*k+i] - s * v[3*k+j];
                            v[3*k+j] = s * v[3*k+i] + c * v[3*k+j];
                            v[3*k+i] = vtemp;
                        }
                        
                        dtemp = c*c*d[i] + s*s*d[j] - 2.0f*c*s*b;
                        d[j] = s*s*d[i] + c*c*d[j] + 2.0f*c*s*b;
                        d[i] = dtemp;
                    }
                }
            }
        }
		Update(d, v);
        return;
    }
	
	private void Update(float[] d, float[] v) {
		eigenValues.x = d[0];
		eigenValues.y = d[1];
		eigenValues.z = d[2];
		
		tmp = new Matrix3x3();
		for (int i = 0; i < 9; ++ i) {
			tmp[i] = v[i];
		}
		
		eigenVectors = tmp.transpose;
	}
}

