# What is this?
This is an implementation of the following paper:

*Yuki Koyama and Takeo Igarashi. 2013. View-dependent control of elastic rod simulation for 3D character animation. In Proceedings of the 12th ACM SIGGRAPH/Eurographics Symposium on Computer Animation (SCA '13). ACM, New York, NY, USA, 73-78.*

Implemented and maintained by Yuki Koyama. (<koyama@is.s.u-tokyo.ac.jp>)

# Project page
<http://www-ui.is.s.u-tokyo.ac.jp/~koyama/project/ViewDependentRodSimulation/>

# Note
1. This is released for non-commercial use only.
1. This implementation is slightly modified from the version of making the demos for the paper.
1. It is not widely tested.
1. This source code is desined as the Unity scripts (tested ond Unity 4.1.3). Other environments are not supported.

# How to use
This source code can be used in Unity. Please see this example project files:

<http://www-ui.is.s.u-tokyo.ac.jp/~koyama/project/ViewDependentRodSimulation/unity_project.zip>

If you want to learn more, please read the source code. (I'm sorry not to write a detailed documentation.)
