using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	public class ExampleBasedRodObject : FKRodObject {
		
		[SerializeField] private List<QuaternionList>	posesEditor			= null;
		[SerializeField] public  List<QuaternionList>	orientationsEditor	= null;
		[SerializeField] private List<Vector3List>		positionsEditor       = null;
				
		public int nPosesEditor {
			get {
				return posesEditor.Count;
			}
		}
		
		public override void InitializeEditor() {
			base.InitializeEditor();
			posesEditor			= new List<QuaternionList>();
			orientationsEditor	= new List<QuaternionList>();
			positionsEditor		= new List<Vector3List>();
			AddPoseEditor();
		}
		
		public virtual void SetRegisteredPoseEditor(int index) {
			if (index >= posesEditor.Count) {
				Debug.Log("Invalid index of pose.");
				return;
			}
			
			int nParticles = particles.Count;
			for (int i = 0; i < nParticles; ++ i) {
				OrientedParticle	p = particles[i];
				
				p.transform.localPosition = positionsEditor[index].vList[i];
				p.transform.localRotation = orientationsEditor[index][i];
			}
		}
		
		public virtual void AddPoseEditor() {
			// get the current pose
			QuaternionList newPose			= GetCurrentPoseEditor();
			QuaternionList newOrientations	= GetCurrentOrientationsEditor();
			
			// register the new pose
			posesEditor.Add(newPose);
			orientationsEditor.Add(newOrientations);
			
			// get raw positions
			Vector3List vList = new Vector3List();
			for (int i = 0; i < particles.Count; ++ i) {
				OrientedParticle p = particles[i];
				vList.vList.Add(p.transform.localPosition);
			}
			positionsEditor.Add(vList);
		}
		
		public virtual void DeletePoseEditor() {
			if (posesEditor.Count <= 1) {
				Debug.Log("Nothing to do.");
				return;
			}
			
			posesEditor.RemoveAt(posesEditor.Count - 1);
			orientationsEditor.RemoveAt(orientationsEditor.Count - 1);
			positionsEditor.RemoveAt(positionsEditor.Count - 1);
		}
		
		protected QuaternionList GetCurrentPoseEditor() {
			int nParticles = particles.Count;
			
			QuaternionList currentPose = new QuaternionList();
			
			Vector3 prevDir = referenceDirection;
			
			for (int index = 0; index < nParticles - 1; ++ index) {
				OrientedParticle p0 = particles[index + 0];
				OrientedParticle p1 = particles[index + 1];
				
				Vector3 vec = p1.transform.localPosition - p0.transform.localPosition;
				Vector3 dir = vec.normalized;
				
				Quaternion rot = QuaternionEx.QuaternionFromVectors(prevDir, dir).convert;
				rot = checkQuaternion(rot);				

				currentPose.Add(rot);

				prevDir = dir;
			}
			
			return currentPose;
		}
		
		protected QuaternionList GetCurrentOrientationsEditor() {
			int nParticles = particles.Count;
			
			QuaternionList ori = new QuaternionList();
			
			for (int i = 0; i < nParticles; ++ i) {
				OrientedParticle p = particles[i];
				Quaternion localOrientation = p.transform.localRotation;
				localOrientation = checkQuaternion(localOrientation);
				
				ori.Add(localOrientation);
			}
			return ori;
		}


		/****************************************************************/
		
		public	  List<float>				weights			= null;
		protected List<Pose>				poses			= null;
		protected List<VectorVL>			descriptors		= null;
		protected List<List<QuaternionEx>>	orientations	= null;

		protected int nPoses {
			get {
				return weights.Count;
			}
		}

		protected override void Initialize() {
			SetRegisteredPoseEditor(0);
			
			base.Initialize();
			weights			= new List<float>();
			poses			= new List<Pose>();
			descriptors		= new List<VectorVL>();
			orientations	= new List<List<QuaternionEx>>();

			for (int i = 0; i < posesEditor.Count; ++ i) {
				// pose
				Pose pose = new Pose();
				pose.Init(posesEditor[i].qList.Count);
				for (int j = 0; j < posesEditor[i].qList.Count; ++ j) {
					Quaternion q = posesEditor[i][j];
					pose.Add (QuaternionEx.Convert(q));
				}
				poses.Add(pose);
				
				// descriptor
				descriptors.Add (CalculateDescriptorFromPose(pose));
				
				// orientation
				List<QuaternionEx> ori = new List<QuaternionEx>();
				for (int j = 0; j < orientationsEditor[i].qList.Count; ++ j) {
					Quaternion q = orientationsEditor[i][j];
					ori.Add(QuaternionEx.Convert(q));
				}
				orientations.Add(ori);
				
				weights.Add(0.0f);
			}
			weights[0] = 1.0f;
		}
		
		public override void ProjectConstraints(float dt, int stiffness) {
			
		    UpdateWeights();
			
			// interpolation
			VectorVL	targetDescriptor	= CalculateInterpolatedDescriptor();
			Pose		targetPose			= CalculatePoseFromDescriptor(targetDescriptor);
			
			List<QuaternionEx> targetOrientations = CalculateInterpolatedOrientations();
			
			UpdateFKParamsWithNewPose(targetPose, targetOrientations);
			UpdateRestStateOfParticlesUsingFKParams();
			
			CalculateStaticVariables();
			
			base.ProjectConstraints(dt, stiffness);
		}
		
		protected void CalculateStaticVariables() {
			foreach (Cluster c in clusters) {
				c.Initialize();
			}
		}
		
		public virtual void AddPose() {

			// get the current pose
			Pose		newPose			= GetCurrentPose();
			
			// make the descriptor of the new pose
			VectorVL	newDescriptor	= CalculateDescriptorFromPose(newPose);
			
			List<QuaternionEx> newOrientations = GetCurrentOrientations();
			
			// register the new pose
			poses.Add(newPose);
			descriptors.Add(newDescriptor);
			orientations.Add (newOrientations);
			weights.Add(0.0f);
		}

		public virtual void DeletePose() {
			if (nPoses <= 1) {
				Debug.Log ("Nothing to do.");
				return;
			}
			
			poses.RemoveAt(poses.Count - 1);
			descriptors.RemoveAt(descriptors.Count - 1);
			orientations.RemoveAt (orientations.Count - 1);
			weights.RemoveAt(weights.Count - 1);
		}
		
		/////////////
		
		// example-based control
		protected void UpdateFKParamsWithNewPose(Pose pose, List<QuaternionEx> orientations) {
			rotationVector	= new List<QuaternionEx>();
		    for (int i = 0; i < pose.size; ++ i) {
				rotationVector.Add (pose.qList[i]);
			}
			
			orientationVector = orientations;
		}
		
		protected virtual void UpdateWeights() {

		}
		
		protected VectorVL CalculateInterpolatedDescriptor() {
			VectorVL target = new VectorVL(descriptors[0].size);
			
			// all descriptors should be "checked" before this method is called.
			for (int j = 0; j < descriptors[0].size / 4; ++ j) {
				QuaternionEx pivot = new QuaternionEx();
				pivot.x = descriptors[0][j * 4 + 0];
				pivot.y = descriptors[0][j * 4 + 1];
				pivot.z = descriptors[0][j * 4 + 2];
				pivot.w = descriptors[0][j * 4 + 3];
				for (int i = 0; i < nPoses; ++ i) {
					VectorVL des = descriptors[i];
					
					QuaternionEx qTarget  = new QuaternionEx();
					QuaternionEx qCurrent = new QuaternionEx();
					
					qTarget.x = target[j * 4 + 0];
					qTarget.y = target[j * 4 + 1];
					qTarget.z = target[j * 4 + 2];
					qTarget.w = target[j * 4 + 3];
					
					qCurrent.x = des[j * 4 + 0];
					qCurrent.y = des[j * 4 + 1];
					qCurrent.z = des[j * 4 + 2];
					qCurrent.w = des[j * 4 + 3];
					
					float	inner	= QuaternionEx.DotProduct(qCurrent, pivot);
					bool	invert	= inner < 0.0;
					if (invert) {
						qCurrent = -1.0f * qCurrent;
					}
					
					qTarget = qTarget + weights[i] * qCurrent;
					
					target[j * 4 + 0] = qTarget.x;
					target[j * 4 + 1] = qTarget.y;
					target[j * 4 + 2] = qTarget.z;
					target[j * 4 + 3] = qTarget.w;
				}
			}
			return target;
		}
		
		protected List<QuaternionEx> CalculateInterpolatedOrientations() {
			List<QuaternionEx> target = new List<QuaternionEx>();
			
			// simply apply QLERP [Kavan05]
			int nQuaternions = particles.Count;
			for (int i = 0; i < nQuaternions; ++ i) {
				QuaternionEx q = QuaternionEx.zero;
				QuaternionEx pivot = orientations[0][i];
				for (int j = 0; j < nPoses; ++ j) {
					
					QuaternionEx tmp = orientations[j][i];
					if (QuaternionEx.DotProduct(pivot, tmp) < 0.0f) {
						tmp = -1.0f * tmp;
					}
					
					q += weights[j] * tmp;
				}
				target.Add(q.normalize);
			}
			
			return target;
		}
 	
		protected Pose GetCurrentPose() {
			int nParticles = particles.Count;
			
			Pose currentPose = new Pose();
			currentPose.Init(nParticles - 1);
			
			Vector3 prevDir = referenceDirection;
			
			for (int index = 0; index < nParticles - 1; ++ index) {
				OrientedParticle p0 = particles[index + 0];
				OrientedParticle p1 = particles[index + 1];
				
				Vector3 vec = p1.x - p0.x;
				Vector3 dir = vec.normalized;
				
				QuaternionEx rot = QuaternionEx.QuaternionFromVectors(prevDir, dir);
				rot = checkQuaternion(rot);						
				
				currentPose.Add(rot);

				prevDir = dir;
			}

			return currentPose;
		}
		
		protected List<QuaternionEx> GetCurrentOrientations() {
			int nParticles = particles.Count;
			
			List<QuaternionEx> ori = new List<QuaternionEx>();
			
			for (int i = 0; i < nParticles; ++ i) {
				OrientedParticle p = particles[i];
				ori.Add(checkQuaternion(p.q));
			}
			return ori;
		}
		
		// static methods
		protected static Pose CalculatePoseFromDescriptor(VectorVL descriptor) {
			int nQuaternions = descriptor.size / 4;
			
			Pose pose = new Pose();
			pose.Init(nQuaternions);
			
			for (int i = 0; i < nQuaternions; ++ i) {
				QuaternionEx q;
				q.x = descriptor[i * 4 + 0];
				q.y = descriptor[i * 4 + 1];
				q.z = descriptor[i * 4 + 2];
				q.w = descriptor[i * 4 + 3];
				pose.Add(q.normalize);
			}
			return pose;
		}
		
		protected static VectorVL CalculateDescriptorFromPose(Pose pose) {
			int nQuaternions = pose.size;
			
			VectorVL vec = new VectorVL(nQuaternions * 4);
			
			for (int i = 0; i < nQuaternions; ++ i) {
				vec[i * 4 + 0] = pose.qList[i].x;
				vec[i * 4 + 1] = pose.qList[i].y;
				vec[i * 4 + 2] = pose.qList[i].z;
				vec[i * 4 + 3] = pose.qList[i].w;
			}
			
			return vec;
		}
	}
}