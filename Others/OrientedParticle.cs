using UnityEngine;
using System.Collections.Generic;
using System;

namespace ViewDependentRodSimulation {

	public class OrientedParticle : MonoBehaviour, IComparable<OrientedParticle> {
		
		public	float	m =	0.01f;
		
		public	Vector3	x = Vector3.zero;
		public	Vector3 v = Vector3.zero;
		public	Vector3 f = Vector3.zero;
		
		public	Vector3 x0 = Vector3.zero;
		public	Vector3 v0 = Vector3.zero;
		public	Vector3 xp = Vector3.zero;
		
		public	QuaternionEx	q	= QuaternionEx.identity;
		public	QuaternionEx	q0	= QuaternionEx.identity;
		public	QuaternionEx	qp	= QuaternionEx.identity;
		
		public	Vector3 w	= Vector3.zero;	// angular velocity
		
		public	Matrix3x3	Ae		= Matrix3x3.identity;
		public	Matrix3x3	mxx0t	= Matrix3x3.identity;
		
		public	int	mainClusterIndex;
		
		public	List<Vector3>	xgs;
		
		private	float	rx;
		private	float	ry;
		private	float	rz;
		
		private List<ContactPoint> contacts;
		
		// collision handling
		public void OnCollisionEnter(Collision collision) {
			contacts = new List<ContactPoint>();
	        foreach (ContactPoint contact in collision.contacts) {
				contacts.Add (contact);
			}
		}
		
		public void OnCollisionStay(Collision collision) {
			contacts = new List<ContactPoint>();
	        foreach (ContactPoint contact in collision.contacts) {
				contacts.Add (contact);
			}
		}
		
		public void OnCollisionExit(Collision collision) {
			contacts = new List<ContactPoint>();
		}
	
		public void Initialize() {
			contacts = new List<ContactPoint>();
			InitializeRadii();
			InitializeTransform();
		}
		
		public void CalculateVelocity(float dt) {
			v += f * dt / m;
		}
		
		public void CalculateEstimatedPosition(float dt) {
			// position
			xp = x + v * dt;
			
			// orientation
			float	mag = w.magnitude;
			if (mag <= 0.001f) {
				qp = q;						// for stability reason
			} else {
				Vector3 dir = w / mag;
				float	ang = mag * dt;
				QuaternionEx	dq	= QuaternionEx.QuaternionFromAxisAndRadian(dir, ang);
				
				qp = dq * q;
			}
			// qp = q;						// if you want to ignore angular velocity (for simplisity)
		}
		
		public void ProjectCollisionConstraints(float dt) {
			
			float scale = 1.0f;
			
			// if the particle has the animated particle attribute, make the constraints soft.
			AnimatedParticle ap = gameObject.GetComponent<AnimatedParticle>();
			if (ap != null) {
				scale -= ap.effect;
			}
			
			foreach (ContactPoint c in contacts) {
				Collider		other	= c.otherCollider;
				SphereCollider	sphere	= c.thisCollider as SphereCollider;
				
				Ray			ray		= new Ray(xp, - c.normal);
				RaycastHit	hitInfo;
				bool		hit		= other.Raycast(ray, out hitInfo, sphere.radius);
				
				if (hit) {
					float	dis	= hitInfo.distance;
					xp += scale * (sphere.radius - dis) * c.normal;
				}
			}
		}
		
		public void CorrectVelocity(float dt) {
			foreach (ContactPoint c in contacts) {
				float s = - Vector3.Dot(v, c.normal);
				if (s > 0.0f) {
					v += 2.0f * s * c.normal;		// todo
				}
			}
		}
		
		public void FinalizeUpdate() {
			transform.position = x;
			transform.rotation = q.convert;
		}

		void InitializeRadii() {
			SphereCollider	c	= collider as SphereCollider;
			
			if (c != null) {
				rx = c.radius;
				ry = c.radius;
				rz = c.radius;
			} else {
				float s = 0.03f;
				rx = s;
				ry = s;
				rz = s;
			}
		}
		
		void InitializeTransform() {
			x0 = transform.position;
			q0 = QuaternionEx.Convert(transform.rotation);
			
			x = x0;
			q = q0;
		}
		
	    public void UpdateMxx0t() {
			Vector3 t = xp * m;
			
			mxx0t.m00 = t.x * x0.x;
			mxx0t.m01 = t.x * x0.y;
			mxx0t.m02 = t.x * x0.z;
			mxx0t.m10 = t.y * x0.x;
			mxx0t.m11 = t.y * x0.y;
			mxx0t.m12 = t.y * x0.z;
			mxx0t.m20 = t.z * x0.x;
			mxx0t.m21 = t.z * x0.y;
			mxx0t.m22 = t.z * x0.z;
		}
		
	    public void UpdateAe() {
			Matrix3x3		tmp = Matrix3x3.identity;
			QuaternionEx	r	= qp * q0.inverse;		// "r" is rotation, "q" is orientation
			Matrix3x3		R	= r.rotationMatrix3x3;
			float			m5	= m * 0.2f;
			
			tmp.m00 = rx * rx * m5;
			tmp.m11 = ry * ry * m5;
			tmp.m22 = rz * rz * m5;
			
			Ae = tmp * R;
		}
		
		// for IComparable interface
		public int CompareTo(OrientedParticle op) {
			return this.name.CompareTo(op.name);
		}
	}
}