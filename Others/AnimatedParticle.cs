using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {

	public class AnimatedParticle : MonoBehaviour {

		public	Transform attached;
		public	float	effect	= 0.8f;

		[SerializeField] private bool isViewDependent = false;
		[SerializeField] private ViewDependentRodObject obj = null;
		[SerializeField] private List<Vector3>		positionList			= null;
		[SerializeField] private List<Quaternion>	orientationList			= null;
		[SerializeField] private List<Vector3>		attachedPositionList	= null;
		[SerializeField] private List<Quaternion>	attachedOrientationList	= null;
		
		public void InitializeEditor() {
			
			obj = transform.parent.GetComponent<ViewDependentRodObject>();
			
			if (obj != null) {
				isViewDependent = true;
				
				positionList	= new List<Vector3>();
				orientationList	= new List<Quaternion>();
				attachedPositionList	= new List<Vector3>();
				attachedOrientationList	= new List<Quaternion>();
			}
		}
		
		public void AddPoseEditor() {
			if (isViewDependent) {
				positionList.Add(transform.localPosition);
				orientationList.Add(transform.localRotation);
				attachedPositionList.Add(attached.transform.localPosition);
				attachedOrientationList.Add(attached.transform.localRotation);
			}
		}
		
		public void DeletePoseEditor() {
			if (isViewDependent) {
				positionList.RemoveAt(positionList.Count - 1);
				orientationList.RemoveAt(orientationList.Count - 1);
				attachedPositionList.RemoveAt(attachedPositionList.Count - 1);
				attachedOrientationList.RemoveAt(attachedOrientationList.Count - 1);
			}
		}
		
		/**************************************************************************/

		private	Vector3			offset;
		private QuaternionEx	initialParentOrientationInverse;
		private QuaternionEx	initialOrientation;
		
		private List<Vector3>		offsetList							= null;
		private List<QuaternionEx>	initialParentOrientationInverseList	= null;
		private List<QuaternionEx>	initialOrientationList				= null;
			
		public void Initialize() {
			if (isViewDependent) {
				offsetList = new List<Vector3>();
				initialParentOrientationInverseList = new List<QuaternionEx>();
				initialOrientationList = new List<QuaternionEx>();
				
				int nPoses = positionList.Count;
				
				for (int i = 0; i < nPoses; ++ i) {
					Vector3 tmp0 = transform.position;
					transform.localPosition = positionList[i];
					Vector3 pos = transform.position;
					transform.position = tmp0;
					
					Vector3 tmp1 = attached.transform.position;
					attached.transform.localPosition = attachedPositionList[i];
					Vector3 attachedPos = attached.transform.position;
					attached.transform.position = tmp1;
					
					Quaternion tmp2 = transform.rotation;
					transform.localRotation = orientationList[i];
					Quaternion ori = transform.rotation;
					transform.rotation = tmp2;
					
					Quaternion tmp3 = attached.transform.rotation;
					attached.transform.localRotation = attachedOrientationList[i];
					Quaternion attachedOri = attached.transform.rotation;
					attached.transform.rotation = tmp3;

					Vector3 _offset		= pos - attachedPos;
					QuaternionEx _initialOrientation = QuaternionEx.Convert(ori);
					QuaternionEx _initialParentOrientationInverse = QuaternionEx.Convert(attachedOri).inverse;
					
					offsetList.Add(_offset);
					initialOrientationList.Add(_initialOrientation);
					initialParentOrientationInverseList.Add(_initialParentOrientationInverse);
				}
			} else {
				offset		= transform.position - attached.position;
				initialOrientation = QuaternionEx.Convert(transform.rotation);
				initialParentOrientationInverse = QuaternionEx.Convert(attached.rotation).inverse;
			}
		}
		
		public Vector3 GetAnimatedPosition() {
			if (isViewDependent) {
				List<float> w = obj.weights;
				Vector3 sum = Vector3.zero;
				for (int i = 0; i < w.Count; ++ i) {
					QuaternionEx rot = QuaternionEx.Convert(attached.rotation) * initialParentOrientationInverseList[i];
					Vector3	newPosition	= attached.position + rot.RotateVector(offsetList[i]);
					sum += w[i] * newPosition;
				}
				
				return sum;

			} else {
				QuaternionEx rot = QuaternionEx.Convert(attached.rotation) * initialParentOrientationInverse;
				Vector3	newPosition	= attached.position + rot.RotateVector(offset);
				return newPosition;
			}
		}
		
		public QuaternionEx GetAnimatedOrientation() {
			if (isViewDependent) {
				List<float> w = obj.weights;
				
				QuaternionEx sum = QuaternionEx.zero;
				for (int i = 0; i < w.Count; ++ i) {
					QuaternionEx rot = QuaternionEx.Convert(attached.rotation) * initialParentOrientationInverseList[i];
					QuaternionEx newOrientation = rot * initialOrientationList[i];
					
					if (QuaternionEx.DotProduct(sum, newOrientation) < 0.0f) {
						newOrientation = -1.0f * newOrientation;
					}

					sum += w[i] * newOrientation;
				}
				
				return sum.normalize;
			} else {
				QuaternionEx newOrientation = QuaternionEx.Convert(attached.rotation) * initialParentOrientationInverse * initialOrientation;
				return newOrientation;
			}
		}
	}
}