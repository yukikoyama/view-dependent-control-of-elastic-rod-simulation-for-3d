using System;

namespace ViewDependentRodSimulation {
	
	public struct Segment {
		public int		index0;
		public int		index1;
		public float	distance;
	}

}