#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public class DrawWindow : EditorWindow {

	public static DrawWindow Init() {
		DrawWindow window = EditorWindow.GetWindow<DrawWindow>("Draw") as DrawWindow;
		
		return window;
	}
	
	void OnGUI() {
		
		// layout
		EditorGUILayout.BeginVertical();
		bool button = GUILayout.Button("OK");
		EditorGUIUtility.RenderGameViewCameras(new Rect(0.0f, 0.0f, 100.0f, 100.0f), false, false);
		EditorGUILayout.EndVertical();
	
		Debug.Log (Event.current.mousePosition);
		// button event
		if (button) {
			Close();
		}
	}
}
#endif
