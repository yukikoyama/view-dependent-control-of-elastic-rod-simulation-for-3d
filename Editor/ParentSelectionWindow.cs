#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ParentSelectionWindow : EditorWindow {

	private	GameObject		fbxObject;
	private	int				nRods			= 1;
	private	List<Transform>	allTransforms	= null;
	private	int[]			selected		= null;
	private	int[]			prevSelected	= null;
	private	string[]		names 			= null;
	private	Vector2[]		scrollPosition	= null;
	
	public static ParentSelectionWindow Init() {
		ParentSelectionWindow window = EditorWindow.GetWindow<ParentSelectionWindow>("Rod") as ParentSelectionWindow;
		
		return window;
	}
	
	void OnGUI() {
		
		// layout
		EditorGUILayout.BeginVertical();
		
		EditorGUILayout.Space();

		for (int i = 0; i < nRods; ++ i) {
			string s = "Please select the kinematic joint connected to " + allTransforms[prevSelected[i]].gameObject.name + ".";
			GUIStyle style = new GUIStyle();
			style.fontStyle = FontStyle.Bold;
			GUILayout.Label(s, style);
			scrollPosition[i]	= GUILayout.BeginScrollView(scrollPosition[i]);
			selected[i]			= GUILayout.SelectionGrid(selected[i], names, 7);
			GUILayout.EndScrollView();
			EditorGUILayout.Separator();
		}
		
		GUILayout.FlexibleSpace();
		bool button = GUILayout.Button("OK");
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
		
		// button event
		if (button) {
			ParameterSettingWindow window = ParameterSettingWindow.Init();
			window.Initialize(fbxObject, nRods, allTransforms, prevSelected, selected, names);
			Close ();
		}
	}
	
	public void Initialize(GameObject fbxObject, int nRods, List<Transform> allTransforms, int[] prevSelected, string[] names) {
		this.fbxObject	= fbxObject;
		this.nRods		= nRods;
		this.allTransforms	= allTransforms;
		this.prevSelected	= prevSelected;
		this.names		= names;
		
		scrollPosition	= new Vector2[nRods];
		selected		= new int[nRods];
		
		// find parents of prevSelected
		for (int i = 0; i < nRods; ++ i) {
			Transform p = allTransforms[prevSelected[i]].parent;
			selected[i] = allTransforms.FindIndex(delegate(Transform t) { return t == p; });
			if (selected[i] < 0) {
				selected[i] = 0;
			}
		}
	}
}
#endif
