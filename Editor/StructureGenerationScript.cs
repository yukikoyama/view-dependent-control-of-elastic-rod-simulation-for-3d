#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using ViewDependentRodSimulation;

public class StructureGenerationScript {

	public	GameObject		fbxObject		= null;
	public	int				nRods			= 0;
	public	List<Transform>	allTransforms	= null;
	public	int[]			roots			= null;
	public	int[]			attaches		= null;
	
	public	bool			attachCollidersToRods	= false;
	public	float			colliderRadius	= 0.0f;
	
	public	bool			attachCollidersToBody	= false;
	public	float			capsuleSize				= 0.0f;
	
	public	int				nAnimatedParticles	= 0;
	
	public	int				rootJoint			= 0;
	
	private	List<Rod>		rods			= null;
	
	public class Rod {
		public	int			rootJoint;
		public	List<int>	allJointsInOrder;		// from root to leaf
		public	string		name;
		public	GameObject	obj;
		public	ViewDependentRodObject	rodObject;
		public	int			index;
		
		private	List<Transform>	allTransforms;
		
		public bool Initialize(int index, int rootJoint, string name, List<Transform> allTransforms) {
			this.index		= index;
			this.rootJoint	= rootJoint;
			this.name		= name;
			this.allTransforms	= allTransforms;
			
			allJointsInOrder = new List<int>();
			
			return Traverse(rootJoint);
		}
		
		private bool Traverse(int index) {
			allJointsInOrder.Add (index);

			Transform t = allTransforms[index];
			if (t.childCount >= 2) {
				// the rod has a branch
				return false;
			} else if (t.childCount == 1) {
				// there is only one child
				Transform child = t.GetChild(0);
				return Traverse(allTransforms.IndexOf(child));
			} else {
				// there is no child
				return true;
			}
		}
	}

	public StructureGenerationScript () {}
	
	public bool Execute() {
		
		rods = new List<Rod>();
		for (int i = 0; i < roots.Length; ++ i) {
			int root = roots[i];
			
			// generate Rod struct
			Rod rod = new Rod();
			string name = "rod_" + root.ToString();		// todo: user setting 
			rod.Initialize(i, root, name, allTransforms);
			rods.Add (rod);
		}
		
		foreach (Rod rod in rods) {
			foreach (int jointIndex in rod.allJointsInOrder) {
				Transform	t	= allTransforms[jointIndex];
				GameObject	g	= t.gameObject;

				// add OrientedParticle to the rod joints
				g.AddComponent<ViewDependentRodSimulation.OrientedParticle>();
				
				if (attachCollidersToRods) {
					
					// add SphereCollider to the rod joints
					SphereCollider sc = g.AddComponent<SphereCollider>();
					sc.radius = colliderRadius;
					
					// add Rigidbody to the rod joints
					Rigidbody rb = g.AddComponent<Rigidbody>();
					rb.isKinematic	= true;
					rb.useGravity	= false;
				}
			}
		}
			
		foreach (Rod rod in rods) {
			
			// generate RodObject for each rod
			GameObject obj = new GameObject(rod.name);
			rod.obj			= obj;
			rod.rodObject	= obj.AddComponent<ViewDependentRodObject>();
			
			for (int i = 0; i < rod.allJointsInOrder.Count; ++ i) {
				int jointIndex = rod.allJointsInOrder[i];
				
				// rename the rod joints
				GameObject joint = allTransforms[jointIndex].gameObject;
				joint.name	= i.ToString("D3") + "_" + rod.name + "_" + joint.name;
				
				// modify the hierarchy between the RodObjects and the rod joints
				joint.transform.parent = rod.obj.transform;
				
				if (i < nAnimatedParticles) {
					
					// add AnimatedParticle to the rod joints
					float s = (float)nAnimatedParticles;
					float x = (s - (float)i) / (s + 1.0f);
					float y = x * x * x * x;				// please customize yourself!
					ViewDependentRodSimulation.AnimatedParticle ap = joint.AddComponent<ViewDependentRodSimulation.AnimatedParticle>();
					ap.effect = y;
					if (i == 0) {
						ap.attached = allTransforms[attaches[rod.index]];
					} else {
						ap.attached = allTransforms[rod.allJointsInOrder[i - 1]];
					}
				}
			}
		}
	
		// generate meta GameObject
		GameObject meta = new GameObject(fbxObject.name);
		
		// add Engine to the meta GameObject
		meta.AddComponent<Engine>();
		
		// modify the hierarchy between the meta GameObject and others
		foreach (Rod rod in rods) {
			rod.obj.transform.parent = meta.transform;
		}
		fbxObject.transform.parent = meta.transform;
		fbxObject.name = fbxObject.name + "_Body";
		
		// add CapsuleCollider to the non-rod joints
		// add Rigidbody to the non-rod joints
		if (attachCollidersToBody) {
			Transform root = allTransforms[rootJoint];
			Traverse (root);
		}
		
		// view-dependent rod object settings
		foreach (Rod rod in rods) {
			rod.rodObject.rootTransform = allTransforms[attaches[rod.index]];
		}
		
		// initialize rod objects
		foreach (Rod rod in rods) {
			rod.rodObject.InitializeEditor();
		}
		
		return true;
	}
	
	private void Traverse(Transform t) {
		
		CapsuleCollider cc	= t.gameObject.AddComponent<CapsuleCollider>();
		cc.radius	= capsuleSize;
		cc.height	= capsuleSize * 2.0f;
		
		Rigidbody rb	= t.gameObject.AddComponent<Rigidbody>();
		rb.useGravity	= false;
		
		int nChildren = t.childCount;
		for (int i = 0; i < nChildren; ++ i) {
			Transform child = t.GetChild(i);
			Traverse(child);
		}
	}
}
#endif