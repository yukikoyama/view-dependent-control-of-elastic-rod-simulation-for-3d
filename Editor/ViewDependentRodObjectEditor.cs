using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	[CustomEditor(typeof(ViewDependentRodObject))]
	[CanEditMultipleObjects]
	public class ViewDependentRodObjectEditor : Editor {
		
		static private bool drawing = false;
		static public List<Vector2> drawnLine = null;
		static private Texture2D tex = null;

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			/*
			if (GUILayout.Button("Add Pose")) {
				foreach (ViewDependentRodObject obj in targets) {
					obj.AddPoseEditor();
				}
			}
			*/
			if (GUILayout.Button("Delete Pose")) {
				foreach (ViewDependentRodObject obj in targets) {
					obj.DeletePoseEditor();
				}
			}

			for (int i = 0 ; i < ((ViewDependentRodObject) target).nPosesEditor; ++ i) {
				if (GUILayout.Button("Set Pose " + i.ToString())) {
					foreach (ViewDependentRodObject obj in targets) {
						obj.SetRegisteredPoseEditor(i);
					}
				}
			}
		}
		
		public void OnSceneGUI() {
		
			ViewDependentRodObject obj = target as ViewDependentRodObject;
			if (obj == null) {
				Debug.Log ("Something wrong");
				return;
			}
			
			// GUI
			Handles.BeginGUI();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(20.0f);
			bool addPoseButton = GUILayout.Button ("Add Pose");
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();			
			Handles.EndGUI();
			
			if (addPoseButton) {
				obj.AddPoseWithCameraPositionEditor(Camera.current.transform.position);
			}
		
			// generate texture for visualization
			if (tex == null) {
				tex = new Texture2D(1, 2);
				tex.hideFlags = HideFlags.HideAndDontSave;
				tex.SetPixel(0, 0, new Color(1.0f, 1.0f, 1.0f, 0.8f));
				tex.SetPixel(0, 1, new Color(1.0f, 1.0f, 1.0f, 1.0f));
				tex.Apply();
			}
			
			bool stopDrawing = false;
			
			if (!drawing) {
				Event e = Event.current;
				if (e.shift) {
					drawing = true;	
					drawnLine = new List<Vector2>();
				}
			} else {
				Event e = Event.current;
				if (!e.shift) {
					drawing = false;
					stopDrawing = true;
				}
			}
			
			// get drawn curve
			if (drawing) {
				Event e = Event.current;
				if (e.isMouse && e.shift) {
					Vector2 p = e.mousePosition;
					if (drawnLine.Count != 0) {
						Vector2 prev = drawnLine[drawnLine.Count - 1];
						if ((p - prev).magnitude > 2.0f) {
							drawnLine.Add(p);
						}
					} else {
						drawnLine.Add(p);
					}
				}
			}

			// draw poly-line
			Handles.color = Color.red;
			List<Vector3> vec = new List<Vector3>();
			foreach (OrientedParticle p in obj.particles) {
				vec.Add(p.transform.position);
			}
			Handles.DrawAAPolyLine(tex, 4.0f, vec.ToArray());
			
			// draw poly-line
			if (drawnLine != null) {
				Handles.color = Color.green;
				List<Vector3> vec3 = new List<Vector3>();
				foreach (Vector2 v in drawnLine) {
					Ray tmpRay = HandleUtility.GUIPointToWorldRay(v);
					vec3.Add(Camera.current.transform.position + tmpRay.direction);
				}
				Handles.DrawAAPolyLine(tex, 4.0f, vec3.ToArray());
			}
			
			// send repaint message
			HandleUtility.Repaint();
					
			// button events
			if (stopDrawing) {
			
				if (drawnLine.Count > 5) {

					// posing
					
					// get the lengths of the rod segments
					List<float> lengths = new List<float>();
					for (int i = 0; i < obj.particles.Count - 1; ++ i) {
						OrientedParticle op0	= obj.particles[i + 0];
						OrientedParticle op1	= obj.particles[i + 1];
						
						float len = (op0.transform.position - op1.transform.position).magnitude;
						lengths.Add(len);
					}
					
					// fix the first particle
					
					// do nothing!
					
					// fix the rest particles
					Vector3 cameraPosition = Camera.current.transform.position;
					int currentDrawnIndex = 0;
					Vector3 previousPosition = obj.particles[0].transform.position;
					QuaternionEx accum = QuaternionEx.identity;
					for (int i = 1; i < obj.particles.Count; ++ i) {
						OrientedParticle opPrev	= obj.particles[i - 1];		// already fixed
						OrientedParticle op		= obj.particles[i];			// not fixed yet;
						Vector3 basePosition = opPrev.transform.position;
						
						// project 2D points to 3D space
						float depth = (cameraPosition - op.transform.position).magnitude;
						List<Vector3> projected = new List<Vector3>();
						for (int j = 0; j < drawnLine.Count; ++ j) {
							Vector2 vec2 = drawnLine[j];
							Ray     ray  = HandleUtility.GUIPointToWorldRay(vec2);
							Vector3 vec3 = cameraPosition + ray.direction * depth;
							
							projected.Add(vec3);
						}
						
						// search the nearest drawn point to the next length
						float  preferredLength = lengths[i - 1];
						int    matchedDrawnIndex = -1;
						float  bestMatchedLengthDiff = Single.MaxValue;
						for (int j = currentDrawnIndex; j < projected.Count; ++ j) {
							Vector3 pos = projected[j];
							float   len = (basePosition - pos).magnitude;
							float   diff = Mathf.Abs(len - preferredLength);
							if (diff < bestMatchedLengthDiff) {
								matchedDrawnIndex = j;
								bestMatchedLengthDiff = diff;
							}
						}
						currentDrawnIndex = matchedDrawnIndex;
						
						// error handling
						if (matchedDrawnIndex < 0) {
							Debug.Log("Invalid Sketch.");
							drawing = false;
							drawnLine = null;
							return;
						}
						
						// calculate rotation
						Vector3      prevDirection = accum.RotateVector((op.transform.position - previousPosition).normalized);
						Vector3      nextDirection = (projected[matchedDrawnIndex] - basePosition).normalized;
						QuaternionEx rotation      = QuaternionEx.QuaternionFromVectors(prevDirection, nextDirection);
						
						accum = rotation * accum;
						previousPosition = op.transform.position;
						
						// get orientation
						QuaternionEx orientation     = accum * QuaternionEx.Convert(op.transform.rotation);
						QuaternionEx prevOrientation = rotation * QuaternionEx.Convert(opPrev.transform.rotation);
						
						// get position
						Vector3 position = basePosition + preferredLength * nextDirection;
						
						// apply the results
						op.transform.position = position;
						op.transform.rotation = orientation.convert;
						opPrev.transform.rotation = prevOrientation.convert;
					}
				}
				
				drawing = false;
				drawnLine = null;
			}
		}
	}
}

