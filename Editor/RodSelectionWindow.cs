#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class RodSelectionWindow : EditorWindow {

	private GameObject 		fbxObject;
	private	int				nRods			= 1;
	
	private List<Transform> allTransforms	= null;
	
	private int[]			selected		= null;
	private string[]		names 			= null;
	private Vector2[]		scrollPosition	= null;
	
	public static RodSelectionWindow Init() {
		RodSelectionWindow window = EditorWindow.GetWindow<RodSelectionWindow>("Rod") as RodSelectionWindow;
		
		return window;
	}
	
	void OnGUI() {
		
		// layout
		EditorGUILayout.BeginVertical();
		
		EditorGUILayout.Space();
		
		for (int i = 0; i < nRods; ++ i) {
			string s = "Please select the root joint of the rod No. " + (i + 1).ToString() + ".";
			GUIStyle style = new GUIStyle();
			style.fontStyle = FontStyle.Bold;
			GUILayout.Label(s, style);
			scrollPosition[i] = GUILayout.BeginScrollView(scrollPosition[i]);
			selected[i] = GUILayout.SelectionGrid(selected[i], names, 7);
			GUILayout.EndScrollView();
			EditorGUILayout.Separator();
		}
		
		GUILayout.FlexibleSpace();
		bool button = GUILayout.Button("OK");
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
		
		// button event
		if (button) {
			ParentSelectionWindow window = ParentSelectionWindow.Init();
			window.position = this.position;
			window.Initialize(fbxObject, nRods, allTransforms, selected, names);
			Close ();
		}
	}
	
	public void Initialize(GameObject fbxObject, int nRods) {
		this.nRods		= nRods;
		
		// instantiate
		this.fbxObject = GameObject.Instantiate(fbxObject) as GameObject;
		this.fbxObject.name = fbxObject.name;
		Animator a;
		if ((a = this.fbxObject.GetComponent<Animator>()) != null) {
			a.animatePhysics = true;
		}
		
		scrollPosition	= new Vector2[nRods];
		selected		= new int[nRods];

		allTransforms = new List<Transform>();
		
		Object[] objs = new Object[1];
		objs[0] = this.fbxObject as Object;
		Object[] results = EditorUtility.CollectDeepHierarchy(objs);
		foreach (Object o in results) {
			GameObject go = o as GameObject;
			if (go != null && go.GetComponent<SkinnedMeshRenderer>() == null) {
				allTransforms.Add (go.transform);
			}
		}
		
		names = new string[allTransforms.Count];
		for (int i = 0; i < allTransforms.Count; ++ i) {
			names[i] = allTransforms[i].name;
		}

	}

}
#endif
