#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public class ErrorWindow : EditorWindow {

	public string message = null;
	
	public static ErrorWindow Init() {
		ErrorWindow window = EditorWindow.GetWindow<ErrorWindow>("Error") as ErrorWindow;
		
		return window;
	}
	
	void OnGUI() {
		
		// layout
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField(message);
		GUILayout.FlexibleSpace();
		bool button = GUILayout.Button("OK");
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
		
		// button event
		if (button) {
			Close();
		}
	}
}
#endif
