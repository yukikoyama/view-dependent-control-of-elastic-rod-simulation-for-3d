using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	[CustomEditor(typeof(OrientedParticle))]
	public class OrientedParticleEditor : Editor {
		
		public override void OnInspectorGUI() {
			DrawDefaultInspector();
		}
		
		public void OnSceneGUI() {
			OrientedParticle particle = target as OrientedParticle;
			ViewDependentRodObject obj = particle.transform.parent.GetComponent<ViewDependentRodObject>();
			
			if (particle == null || obj == null) {
				Debug.Log ("Something wrong");
				return;
			}
			
			int index = obj.particles.IndexOf(particle);
			
			Vector3 center = particle.transform.position;
			
			// get orientations and rotations
			QuaternionEx before = QuaternionEx.Convert(particle.transform.rotation);
			QuaternionEx after  = QuaternionEx.Convert(Handles.RotationHandle(particle.transform.rotation, particle.transform.position));
			QuaternionEx delta  = after * before.inverse;
			
			// set rotations for the rest particles
			for (int i = index; i < obj.particles.Count; ++ i) {
				OrientedParticle p = obj.particles[i];
				
				// update orientation
				QuaternionEx ori = QuaternionEx.Convert(p.transform.rotation);
				p.transform.rotation = (delta * ori).convert;
				
				// update position
				Vector3 oldPos = p.transform.position;
				Vector3 oldDir = oldPos - center;
				Vector3 newDir = delta.RotateVector(oldDir);
				Vector3 newPos = center + newDir;
				p.transform.position = newPos;
			}
			
			// draw spheres for the rest particles
			for (int i = index; i < obj.particles.Count; ++ i) {
				OrientedParticle p = obj.particles[i];
				Handles.SphereCap(0, p.transform.position, p.transform.rotation, 0.01f);
			}
			
			// draw poly-line
			Handles.color = Color.red;
			List<Vector3> vec = new List<Vector3>();
			foreach (OrientedParticle p in obj.particles) {
				vec.Add(p.transform.position);
			}
			Handles.DrawPolyLine(vec.ToArray());
		}
	}
}

