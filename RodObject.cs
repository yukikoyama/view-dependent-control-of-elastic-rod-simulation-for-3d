using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {

public class RodObject : BaseObject {
	
		public	int		stiffnessW		= 1;		// corresponds to width in [Rivers 2007]. Don't make this big.
		public	float	stiffnessA		= 0.2f;		// corresponds to alpha in [Muller 2005]
		public	float	inextensibility = 0.99f;
		public	float	torsionResistance = 0.1f;
		public	float	damping			= 0.05f;
		public	float	drag			= 0.005f;

		public	List<OrientedParticle>	particles		= null;
		public	List<AnimatedParticle>	animParticles	= null;
		[SerializeField] protected List<Cluster>		clusters		= null;
		private List<Segment>			segments		= null;

		public virtual void InitializeEditor() {
			MakeParticlesArrayEditor();
			MakeClustersArray();
			
			InitializeParticlesEditor();
		}
		
		protected void InitializeParticlesEditor() {
			foreach (AnimatedParticle ap in animParticles) {
				ap.InitializeEditor();
			}
		}

		
		protected override void Start () {
			base.Start();
			Initialize();
		}
		
		public override void CalculateForces(float dt) {
			// clear
			foreach (OrientedParticle p in particles) {
				p.f = Vector3.zero;
			}
			
			// gravity
			if (useGravity) {
				foreach (OrientedParticle p in particles) {
					p.f.y += engine.gravity * p.m;
				}
			}
		}
		
		public override void CalculateVelocities(float dt) {
			foreach (OrientedParticle p in particles) {
				p.CalculateVelocity(dt);
			}
		}
		
		// [Muller 2007]
		public override void DampVelocities(float dt) {
			foreach (OrientedParticle p in particles) {
				p.v *= 1.0f - drag;
			}
			
			Vector3 xcm = Vector3.zero;
			Vector3 vcm = Vector3.zero;
			
			float	m	= 0.0f;
			
			foreach (OrientedParticle p in particles) {
				xcm	+= p.x * p.m;
				vcm	+= p.v * p.m;
				m	+= p.m;
			}
			
			xcm *= 1.0f / m;
			vcm *= 1.0f / m;
			
			Vector3		L = Vector3.zero;
			Matrix3x3	I = Matrix3x3.zero;
			
			foreach (OrientedParticle p in particles) {
				Vector3	Ri	= p.x - xcm;
				Vector3 tmp	= Vector3.Cross(Ri, (p.m * p.v));
				
				L += tmp;
				
				Matrix3x3 RTilde	= Matrix3x3.zero;
				RTilde.m01 = - Ri.z;
				RTilde.m02 = + Ri.y;
				RTilde.m10 = + Ri.z;
				RTilde.m12 = - Ri.x;
				RTilde.m20 = - Ri.y;
				RTilde.m21 = + Ri.x;
				
				Matrix3x3 tmpM	= RTilde * RTilde.transpose;
				
				I += p.m * tmpM;
			}
			
			Vector3 w = I.inverse * L;
			
			foreach (OrientedParticle p in particles) {
				Vector3 Ri	= p.x - xcm;
				Vector3 DVi	= vcm + Vector3.Cross(w, Ri) - p.v;
				
				p.v += damping * DVi;
			}
		}
		
		public override void CalculateEstimatedPositions(float dt) {
			foreach (OrientedParticle p in particles) {
				p.CalculateEstimatedPosition(dt);
			}
		}
			
		public override void ProjectConstraints(float dt, int stiffness) {
			for (int i = 0; i < stiffness; ++ i) {
					ProjectCollisionConstraints(dt);
					ProjectElasticConstraints(dt);
					ProjectTorsionResistanceConstraints(dt);
					ProjectDistanceConstraints(dt);
					ProjectAnimationConstraints(dt);
				}
			}
		
		public override void ApplyEstimatedPositions(float dt) {
			foreach (OrientedParticle p in particles) {
				// velocity
				p.v = (p.xp - p.x) / dt;
				// position
				p.x = p.xp;
				
				// angular velocity
				QuaternionEx	qp		= p.qp;
				QuaternionEx	qinv	= p.q.inverse;
				float			dot		= QuaternionEx.DotProduct(qp, qinv);
				
				if (dot < 0.0f) {
					qp = -1.0f * qp;
				}
				
				QuaternionEx	tempQ	= qp * qinv;
				Vector3			axis	= tempQ.axis;
				float			angle	= tempQ.angle;
				if (angle < 0.0001f) {
					p.w = Vector3.zero;
				} else {
					p.w = (angle / dt) * axis;
				}
				// orientation
				p.q = p.qp.normalize;
			}
		}
	
		public override void CorrectVelocities(float dt) {
			foreach (OrientedParticle p in particles) {
				p.CorrectVelocity(dt);
			}
		}
	
		public override void FinalizeUpdate() {
			foreach (OrientedParticle p in particles) {
				p.FinalizeUpdate();
			}
		}
		
		protected void ProjectCollisionConstraints(float dt) {
			foreach (OrientedParticle p in particles) {
				p.ProjectCollisionConstraints(dt);
			}
		}
		
		protected void ProjectElasticConstraints(float dt) {
			
			// update particles
			foreach (OrientedParticle p in particles) {
				p.UpdateAe();
				p.UpdateMxx0t();
				p.xgs = new List<Vector3>();
			}
			
			// update clusters
			foreach (Cluster c in clusters) {
				c.UpdateCurrentCenterPosition();
			}
			
			// do shape-matching algorithm
			foreach (Cluster c in clusters) {
				Matrix3x3	A	= c.GetMomentMatrix();
				Matrix3x3	ATA	= A.transpose * A;
				Matrix3x3	W	= c.Eigen.transpose * ATA * c.Eigen;	// warm start [Rivers 2007]
	
				Vector3 eigenValues = new Vector3();
				
				Jacobi j = new Jacobi();
				j.eigenValues	= eigenValues;
				j.eigenVectors	= c.Eigen;
				j.matrix		= W;
				j.Do();
				eigenValues	= j.eigenValues;
				c.Eigen		= j.eigenVectors;
				
				for (int i = 0; i < 3; ++ i) {
					if (eigenValues[i] <= 0.0f) {
						eigenValues[i] = 0.0001f;
					}
					eigenValues[i] = 1.0f / Mathf.Sqrt(eigenValues[i]);
				}
				
				Matrix3x3 DPrime = Matrix3x3.identity;
				DPrime.m00 = eigenValues[0];
				DPrime.m11 = eigenValues[1];
				DPrime.m22 = eigenValues[2];
				
				Matrix3x3 SInverse	= c.Eigen * DPrime * c.Eigen.transpose;
				Matrix3x3 R			= A * SInverse;
				
				if (R.determinant < 0.0f) {
					R = - 1.0f * R;
				}
				
				c.R = R;
			}
			
			// calculate and gather goal positions
			foreach (Cluster c in clusters) {
				foreach (int pIndex in c.particleIndices) {
					OrientedParticle p = particles[pIndex];
					Vector3 gx = c.R * (p.x0 - c.c0) + c.c;
					p.xgs.Add(gx);
				}
			}
	
			// apply it
			foreach (OrientedParticle p in particles) {
				// position
				Vector3 sum		= Vector3.zero;
				float	scale	= 1.0f / (float) p.xgs.Count;
				foreach (Vector3 xg in p.xgs) {
					sum += scale * stiffnessA * (xg - p.xp);
				}
				
				p.xp += sum;
				
				// orientation
				Matrix3x3		rotation	= clusters[(int)p.mainClusterIndex].R;
				QuaternionEx	orientation	= QuaternionEx.QuaternionFromMatrix(rotation) * p.q0;
				
				p.qp = orientation;
			}
		}
			
		protected void ProjectTorsionResistanceConstraints(float dt) {
			float s = torsionResistance;
			
			foreach (Segment seg in segments) {
				OrientedParticle p0 = particles[seg.index0];
				OrientedParticle p1 = particles[seg.index1];
				
				QuaternionEx rot0 = p0.qp * p0.q0.inverse;
				QuaternionEx rot1 = p1.qp * p1.q0.inverse;
				
				if (QuaternionEx.DotProduct(rot0, rot1) < 0.0f) {
					rot1 = -1.0f * rot1;
				}
				
				QuaternionEx constrainedRot0 = QuaternionEx.Slerp(rot0, rot1, s);
				QuaternionEx constrainedRot1 = QuaternionEx.Slerp(rot1, rot0, s);
				
				p0.qp = (constrainedRot0 * p0.q0).normalize;
				p1.qp = (constrainedRot1 * p1.q0).normalize;
			}
		}
		
		protected void ProjectDistanceConstraints(float dt) {
			float s = inextensibility;
			
			foreach (Segment seg in segments) {
				OrientedParticle p0 = particles[seg.index0];
				OrientedParticle p1 = particles[seg.index1];
				
				Vector3	dir = p0.xp - p1.xp;
				float	len	= dir.magnitude;
				float	w0	= 1.0f / p0.m;
				float	w1	= 1.0f / p1.m;
				float	sum	= w0 + w1;
				Vector3	dx0	= - (1.0f / sum) * (len - seg.distance) * (dir / len) * s;
				Vector3	dx1	= - dx0;
				
				p0.xp += dx0 * w0;
				p1.xp += dx1 * w1;
			}
		}
		
		protected void ProjectAnimationConstraints(float dt) {
			foreach (AnimatedParticle ap in animParticles) {
				OrientedParticle op = ap.GetComponent<OrientedParticle>();
				if (op == null) {
					Debug.Log("Error!");
				}
			
				op.xp = op.xp * (1.0f - ap.effect) + ap.effect * ap.GetAnimatedPosition();
				
				QuaternionEx animatedOrientation = ap.GetAnimatedOrientation();
				if (QuaternionEx.DotProduct(op.qp, animatedOrientation) < 0.0f) {
					animatedOrientation = -1.0f * animatedOrientation;
				}
				op.qp = QuaternionEx.Lerp(op.qp, animatedOrientation, ap.effect);
			}
		}
		
		protected virtual void Initialize() {
			InitializeParticles();
			InitializeClusters();
			
			MakeSegmentsArray();
		}
		
		private void MakeParticlesArrayEditor() {
			particles		= new List<OrientedParticle>();
			animParticles	= new List<AnimatedParticle>();
			
			foreach (Transform t in transform) {
				OrientedParticle p = t.GetComponent<OrientedParticle>();
				if (p != null) {
					particles.Add(p);
				}
				
				AnimatedParticle aP = t.GetComponent<AnimatedParticle>();
				if (aP != null) {
					animParticles.Add(aP);
				}
			}
			
			particles.Sort();
		}
		
		private void MakeClustersArray() {
			clusters = new List<Cluster>();
			int nParticles = particles.Count;
			
			for (int i = 0; i < nParticles; ++ i) {
				OrientedParticle p = particles[i];
				p.mainClusterIndex = i;
				
				Cluster c = new Cluster(this);
		        for (int j = i - stiffnessW; j <= i + stiffnessW; ++ j) {
					if (j >= 0 && j <= nParticles - 1) {
						c.particleIndices.Add(j);
					}
				}
				clusters.Add(c);
			}
		}
		
		private void MakeSegmentsArray() {
			segments = new List<Segment>();
			
			int nSegments = particles.Count - 1;
			
			for (int i = 0; i < nSegments; ++ i) {
				OrientedParticle p0 = particles[i + 0];
				OrientedParticle p1 = particles[i + 1];
	
				float dis = (p1.x0 - p0.x0).magnitude;
	
				Segment	seg;
				seg.index0		= i + 0;
				seg.index1		= i + 1;
				seg.distance	= dis;
				segments.Add(seg);
			}
		}
		
		private void InitializeParticles() {
			foreach (OrientedParticle op in particles) {
				op.Initialize();
			}
			
			foreach (AnimatedParticle ap in animParticles) {
				ap.Initialize();
			}
		}
		
		private void InitializeClusters() {
			foreach (Cluster c in clusters) {
				c.Initialize();
			}
		}
	
	}
		
}